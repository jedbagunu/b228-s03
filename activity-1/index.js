console.log("Hello World")
activity
// 1. What is the blueprint where objects are created from?

// Class
// 2. What is the naming convention applied to classes?
//  Upper case first letter

// 3. What keyword do we use to create objects from a class?
// new

// 4. What is the technical term for creating an object from a class?
// instantiation

// 5. What class method dictates HOW objects will be created from that class?
// constructor
class Student{
    // creating a constructor

    constructor(name, email, grades){
        this.name = name;
        this.email = email;
        this.grade = (grades.every(grade => (grade >= 0 && grade <= 100)) && grades.length === 4)? grades : undefined;
    }

}

let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);